<?php

namespace Drupal\tmgmt_google_v3;

use Drupal\tmgmt\TranslatorPluginUiBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Google V3 Translator translator UI.
 */
class GoogleV3TranslatorUi extends TranslatorPluginUiBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $form_state->getFormObject()->getEntity();
    $form['location'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Location'),
      '#description' => $this->t('Default: global'),
      '#maxlength' => 60,
      '#size' => 60,
      '#default_value' => $translator->getSetting('location'),
    ];

    $form['api_project'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Project ID'),
      '#description' => $this->t('Your Google Cloud Platform project ID'),
      '#maxlength' => 256,
      '#size' => 128,
      '#default_value' => $translator->getSetting('api_project'),
    ];

    $form['google_credentials'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Google API Credentials'),
      '#description' => $this->t('https://cloud.google.com/translate/docs/setup#php'),
      '#default_value' => $translator->getSetting('google_credentials'),
      '#upload_location' => 'private://',
      '#upload_validators' => [
        'file_validate_extensions' => ['json'],
      ],
    ];

    // Create a form field for each language to map
    // a glossary if it exists.
    $form['glossary_mappings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Glossary mappings'),
      '#tree' => TRUE,
    ];

    foreach (\Drupal::languageManager()->getLanguages() as $langcode => $language) {
      $form['glossary_mappings'][$langcode] = [
        '#type' => 'textfield',
        '#title' => $this->t('Glossary for @language', ['@language' => $language->getName()]),
        '#description' => $this->t('Enter the glossary id for language: @langcode', ['@langcode' => $langcode]),
        '#default_value' => $translator->getSetting('glossary_mappings')[$langcode] ?? NULL,
      ];
    }

    $form += parent::addConnectButton();
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    $settings = $form_state->getValues()['settings'];
    if (!empty($settings['google_credentials'])) {
      $credentialsFid = $settings['google_credentials'][0];
      /** @var \Drupal\file\Entity\File $file */
      $file = \Drupal::service('entity_type.manager')->getStorage('file')->load($credentialsFid);
      if ($file && $file->isTemporary()) {
        $file->setPermanent();
        $file->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConnect(array $form, FormStateInterface $form_state) {
    $translator = $form_state->getFormObject()->getEntity();
    $languages = $translator->getPlugin()->getSupportedRemoteLanguages($translator);
    if (!empty($languages)) {
      $this->messenger()->addStatus($this->t('Successfully connected!'));
    }
    else {
      $this->messenger()->addStatus($this->t('Error connecting!'));
    }
  }

}
