<?php

namespace Drupal\tmgmt_google_v3\Plugin\tmgmt\Translator;

use Drupal\Component\Utility\Html;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\tmgmt\ContinuousTranslatorInterface;
use Drupal\tmgmt\Data;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\TranslatorInterface;
use Drupal\tmgmt\TranslatorPluginBase;
use Google\Cloud\Translate\V3\TranslateTextGlossaryConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\tmgmt\Translator\AvailableResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Google\Cloud\Translate\V3\TranslationServiceClient;
use Drupal\tmgmt\Entity\Job;
use Drupal\tmgmt\TMGMTException;

/**
 * Google V3 translator plugin.
 *
 * @TranslatorPlugin(
 *   id = "google_v3",
 *   label = @Translation("Google V3"),
 *   description = @Translation("Google V3 Translator service."),
 *   ui = "Drupal\tmgmt_google_v3\GoogleV3TranslatorUi",
 *   logo = "icons/google.svg",
 * )
 */
class GoogleV3Translator extends TranslatorPluginBase implements ContainerFactoryPluginInterface, ContinuousTranslatorInterface {

  /**
   * Data help service.
   *
   * @var \Drupal\tmgmt\Data
   */
  protected $dataHelper;

  /**
   * Google\Cloud\Translate\V3\TranslationServiceClient definition.
   *
   * @var \Google\Cloud\Translate\V3\TranslationServiceClient
   */
  protected $translationServiceClient;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\tmgmt\Data $data_helper
   *   Data helper service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   This is the service that allows us to load and save entities.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    array $plugin_definition,
    Data $data_helper,
    EntityTypeManagerInterface $entity_type_manager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->dataHelper = $data_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('tmgmt.data'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Initializes the Google translation service client for plugin.
   *
   * @param \Drupal\tmgmt\TranslatorInterface $translator
   *   The translator used to retrieve the 'google_credentials' settings.
   */
  protected function initializeClient(TranslatorInterface $translator) {
    $credentials = $translator->getSetting('google_credentials');
    if (isset($credentials['fids'])) {
      $fid = $credentials['fids'][0];
    }
    if (!isset($fid)) {
      $fid = $credentials[0] ?? NULL;
    }
    if ($fid) {
      $credentialsFile = $this->entityTypeManager->getStorage('file')->load($fid);
    }
    if (!isset($credentialsFile)) {
      return FALSE;
    }
    $absolute_path = \Drupal::service('file_system')->realpath($credentialsFile->getFileUri());
    $options = [
      'credentials' => $absolute_path,
    ];
    $this->translationServiceClient = new TranslationServiceClient($options);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function checkAvailable(TranslatorInterface $translator) {
    if ($translator->getSetting('location') && $translator->getSetting('api_project') && $translator->getSetting('google_credentials')) {
      return AvailableResult::yes();
    }
    return AvailableResult::no($this->t('@translator is not available. Make sure it is properly <a href=:configured>configured</a>.', [
      '@translator' => $translator->label(),
      ':configured' => $translator->toUrl()->toString(),
    ]));
  }

  /**
   * {@inheritdoc}
   */
  public function requestTranslation(JobInterface $job) {
    $this->requestJobItemsTranslation($job->getItems());
    if (!$job->isRejected()) {
      $job->submitted('The translation job has been submitted.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedRemoteLanguages(TranslatorInterface $translator) {
    $remoteLanguages = [];
    if ($this->initializeClient($translator)) {
      $parent = $translator->getSetting('api_project');
      $location = $translator->getSetting('location');
      try {
        $result = $this->translationServiceClient->getSupportedLanguages('projects/' . $parent . '/locations/' . $location);
        /** @var \Google\Cloud\Translate\V3\SupportedLanguages $languages*/
        $languages = $result->getLanguages();
        if (!empty($languages)) {
          $totalLanguages = $languages->count();
          for ($i = 0; $i < $totalLanguages; $i++) {
            /** @var \Google\Cloud\Translate\V3\SupportedLanguage $language*/
            $language = $languages->offsetGet($i);
            $remoteLanguages[$language->getLanguageCode()] = $language->getLanguageCode();
          }
        }
      }
      catch (\Exception $e) {
        \Drupal::messenger()->addMessage($e->getMessage(), 'error');
        return [];
      }
    }
    return $remoteLanguages;
  }

  /**
   * {@inheritdoc}
   */
  public function hasCheckoutSettings(JobInterface $job) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function requestJobItemsTranslation(array $job_items) {
    /** @var \Drupal\tmgmt\Entity\Job $job */
    $job = reset($job_items)->getJob();
    foreach ($job_items as $job_item) {
      if ($job->isContinuous()) {
        $job_item->active();
      }
      // Pull the source data array through the job and flatten it.
      $data = $this->dataHelper->filterTranslatable($job_item->getData());
      $translation = [];
      try {
        foreach ($data as $key => $value) {
          $result = $this->googleTranslateRequestTranslation($job, $value['#text']);
          if (!empty($result)) {
            $translation[$key]['#text'] = Html::decodeEntities($result);
          }
        }
        // Save the translated data through the job.
        // Only reached when all translation requests have succeeded.
        $job_item->addTranslatedData($this->dataHelper
          ->unflatten($translation));
      }
      catch (TMGMTException $e) {
        $job->rejected('Translation has been rejected with following error: @error',
          ['@error' => $e->getMessage()], 'error');
      }
    }
  }

  /**
   * Helper method to do translation request.
   *
   * @param \Drupal\tmgmt\Entity\Job $job
   *   TMGMT Job to be used for translation.
   * @param array|string $text
   *   Text/texts to be translated.
   *
   * @return string
   *   The translated text.
   */
  protected function googleTranslateRequestTranslation(Job $job, $text) {
    $translator = $job->getTranslator();
    $this->initializeClient($translator);
    $language_from = $job->getRemoteSourceLanguage();
    $language_to = $job->getRemoteTargetLanguage();
    $project_id = $translator->getSetting('api_project');
    $location = $translator->getSetting('location');
    $glossary_mappings = $translator->getSetting('glossary_mappings');
    $translation = $this->executeTranslation($language_from, $language_to, $text, $project_id, $location, $glossary_mappings);
    return $translation;
  }

  /**
   * Translates text using the Google Cloud Translation API.
   *
   * @param string $language_from
   *   The language code for the source language from which the text is
   *   being translated.
   * @param string $language_to
   *   The language code for the target language from which the text is
   *   being translated.
   * @param string $text
   *   The text to be translated.
   * @param string $project_id
   *   The Google api project id.
   * @param string $location
   *   The Google api location.
   * @param null|array $glossary_mappings
   *   The glossary mappings or null if it
   *   hasn't yet been set.
   *
   * @return string
   *   The translated text.
   */
  protected function executeTranslation($language_from, $language_to, $text, $project_id, $location, ?array $glossary_mappings = NULL) {
    if (mb_strlen($text) > 20000) {
      return $this->executeTranslation($language_from, $language_to, mb_substr($text, 0, 20000), $project_id, $location) . $this->executeTranslation($language_from, $language_to, mb_substr($text, 20000), $project_id, $location);
    }
    else {
      try {
        $contents = [$text];
        $formatted_parent = $this->translationServiceClient->locationName($project_id, $location);
        $options = [
          'sourceLanguageCode' => $language_from,
        ];
        if ($glossary_mappings && !empty($glossary_mappings[$language_to])) {
          $glossaryConfig = new TranslateTextGlossaryConfig(
            [
              'glossary' => 'projects/' . $project_id . '/locations/' . $location . '/glossaries/' . $glossary_mappings[$language_to],
              'ignore_case' => TRUE,
            ],
          );
          $options += [
            'glossaryConfig' => $glossaryConfig,
          ];
        }
        $response = $this->translationServiceClient->translateText($contents, $language_to, $formatted_parent, $options);
        // If a glossary definition exists use that, otherwise
        // fallback to default translation.
        if ($response->getGlossaryTranslations()->offsetExists(0)) {
          $translation = $response->getGlossaryTranslations()->offsetGet(0)->getTranslatedText();
        }
        else {
          $translation = $response->getTranslations()->offsetGet(0)->getTranslatedText();
        }
      }
      catch (\Throwable $th) {
        throw new TMGMTException($th);
      }
      finally {
        $this->translationServiceClient->close();
      }
    }
    return $translation ?? $text;
  }

}
