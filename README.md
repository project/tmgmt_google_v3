## INTRODUCTION

Google V3 translator plugin for the Translation Management
Tools (TMGMT) project. Allows to use machine translation provided by
Google V3 api to translate content.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/tmgmt_google_v3).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/tmgmt_google_v3)

## REQUIREMENTS

- Depends on Translation Management Tools (http://drupal.org/project/tmgmt).
- Create a Google translation v3 Api
  (https://cloud.google.com/translate/docs/setup) and upload the credentials
  in the plugin configuration.
- For security reasons the credentials file is stored in the private file
  system so you will need to enable it.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## MAINTAINERS

Current maintainers for Drupal 10:

- Paulo Calado - [kallado](https://www.drupal.org/u/kallado)

This project has been sponsored by:

- Visit: [Javali](https://www.javali.pt) for more information
